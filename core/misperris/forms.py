from django import forms
from .models import registroMascota, registroUsuario


class registroMascotaForm(forms.ModelForm):

    class Meta:
        model = registroMascota

        fields = [
            'autor',
            'nombre' ,
            'raza_predominante', 
            'descripcion',
            'estado',
            'imagen',
            'fecha_publicacion',
        ]
        labels = {
            'autor': 'Autor',
            'nombre': 'Nombre' ,
            'raza_predominante': 'Raza Predominante', 
            'descripcion': 'Descripcion',
            'estado': 'Estado',
            'imagen': 'imagen',
            'fecha_publicacion': 'Fecha Publicacion',
        }
        widgets = {
            'autor': forms.TextInput(attrs={'class':'form-control'}),
            'nombre': forms.TextInput(attrs={'class':'form-control'}),
            'raza_predominante': forms.TextInput(attrs={'class':'form-control'}), 
            'descripcion': forms.TextInput(attrs={'class':'form-control'}),
            'estado': forms.Select(attrs={'class':'form-control'}), 
            'imagen': forms.FileField(),
            'fecha_publicacion': forms.DateTimeInput(attrs={'class':'form-control'}),
        }

