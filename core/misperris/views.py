# # -*- coding: utf-8 -*-
# from __future__ import unicode_literals

# from django.shortcuts import render
# from django.utils import timezone
# from .models import registroMascota 

# def index(request):
#     mascotas = registroMascota.objects.order_by('fecha_publicacion')
#     return render(request, 'misperris/index.html', { 'mascotas': mascotas })

# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.utils import timezone
from .models import registroMascota, registroUsuario
from .forms import registroMascotaForm

def index(request):
    mascotas = registroMascota.objects.order_by('fecha_publicacion')
    usuarios = registroUsuario.objects.order_by('nombre')
    return render(request, 'misperris/index.html', { 'mascotas': mascotas , 'usuarios': usuarios})

def mascota_view(request):
    if request.method == 'POST':
        form = registroMascotaForm(request.POST)
        if form.is_valid():
            form.save()
        return redirect('misperris:index')
    else:
        form = registroMascotaForm()
    
    return render(request, 'misperris/index.html', {'form':form})

# def usuario_view(request):
#     if request.method == 'POST':
#         form = registroUsuario(request.POST)
#         if form.is_valid():
#             form.save()
#         return redirect('misperris:index')
#     else:
#         form = registroUsuario()
    
#     return render(request, 'misperris/index.html', {'form':form})